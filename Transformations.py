import torch

        
class AdaptivePad1d(torch.nn.Module):
    def __init__(self, max_size, value=0):
        super().__init__()
        self.__max_size = max_size
        self.__value = value
        
    def forward(self, x):
        return torch.nn.ConstantPad1d((max(0, self.__max_size-x.shape[1]), 0), value=self.__value)(x)

    
class LeftCrop(torch.nn.Module):
    def __init__(self, size):
        super().__init__()
        self.__size = int(size)
        
    def forward(self, x):
        return x[:, :self.__size]

    
class RightCrop(torch.nn.Module):
    def __init__(self, size):
        super().__init__()
        self.__size = int(size)
        
    def forward(self, x):
        return x[:, self.__size:]

    
class CenterCrop(torch.nn.Module):
    def __init__(self, size):
        super().__init__()
        self.__size = int(size)
        
    def forward(self, x):
        sample_size = x.shape[1]
        left_boundry = (sample_size - self.__size) // 2
        right_boundry = left_boundry + self.__size
        return x[:, left_boundry:right_boundry]


class AppendChannel(torch.nn.Module):
    def __init__(self):
        super().__init__()
        
    def forward(self, x):
        return torch.cat([x, torch.zeros(x.shape[1:]).unsqueeze(0)], 0)

class Transpose(torch.nn.Module):
    def __init__(self):
        super().__init__()
        
    def forward(self, x):
        return torch.transpose(x, 0, 1)

    
class Compose(torch.nn.Module):
    def __init__(self, transforms):
        super().__init__()
        self.__transforms = transforms

    def forward(self, x):
        for t in self.__transforms:
            x = t(x)
        return x