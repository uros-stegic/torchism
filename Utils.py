import matplotlib.pyplot as plt
import numpy as np
import os
from datetime import date
import seaborn as sns
import pandas as pd
from sklearn.metrics import confusion_matrix
import torch


def imshow(img):
    npimg = img.numpy()
    plt.imshow(npimg, cmap='gray')
    plt.show()
    

def train(model,
          train_loader,
          loss_fn,
          optimizer,
          epochs,
          val_loader=None, val_count=None,
          model_name=None, model_dir=None,
          device=None,
          start_epoch=0):

    device = device if device is not None else torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    train_loss_history = (epochs*len(train_loader))*[0]
    val_loss_history = (epochs*len(train_loader))*[0]
    today = date.today().strftime('%Y-%m-%d')
    for epoch_idx in range(epochs):
        epoch = epoch_idx + start_epoch
        for i, data in enumerate(train_loader):
            X, y = data
            optimizer.zero_grad()
            preds = model(X.to(device))
            loss = loss_fn(preds, y.to(device))
            loss.backward()
            optimizer.step()
            loss_value = loss.item() / train_loader.batch_size
            train_loss_history[epoch_idx*len(train_loader) + i] = loss_value

            print(f'Epoch {epoch} | Iteration {i:5} | batch loss: {loss_value}', end='\r', flush=True)
            if val_loader is not None and val_count is not None and i > 0 and i % val_count == 0:
                with torch.no_grad():
                    total_loss = .0
                    for X, y in val_loader:
                        preds = model(X.to(device))
                        loss = loss_fn(preds, y.to(device))
                        total_loss += loss.item()/val_loader.batch_size
                    total_loss /= len(val_loader)
                    val_loss_history[epoch_idx*len(train_loader) + i] = total_loss

        if model_dir is not None and model_name is not None:
            model_path = os.path.join(model_dir, f'{model_name}_{epoch:02}_{today}.pt')
            torch.save(model, model_path)
        print(f'', flush=True)
    return model, np.array(train_loss_history), np.array(val_loss_history)


def show_validation(cm, classes):
    plt.figure(figsize=(12,8))
    ax= plt.subplot()
    sns.heatmap(cm, annot=True, ax = ax)

    ax.set_xlabel('True labels')
    ax.set_ylabel('Predicted labels')
    ax.set_title('Confusion Matrix')
    plt.xticks(rotation='vertical')
    plt.yticks(rotation=0)
    ax.xaxis.set_ticklabels(classes)
    ax.yaxis.set_ticklabels(classes)

    prec = [cm[i, i] / cm[i, :].sum() for i in range(cm.shape[0])]
    rec = [cm[i, i] / cm[:, i].sum() for i in range(cm.shape[0])]
    display(pd.DataFrame(data=np.stack([classes, prec, rec]).T, columns=['Genre', 'Precision', 'Recall']))
    
    
def evaluate(model, dataset, classes, device=None):
    device = device if device is not None else torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    with torch.no_grad():
        y_true = []
        y_pred = []
        for X, y in dataset:
            y_true.append(y)
            y_pred.append(model(X.to(device)).argmax().item())

        y_true = np.array(y_true)
        y_pred = np.array(y_pred)

        cm = confusion_matrix(y_true, y_pred).T
        show_validation(cm, classes)


def plot_loss(loss_train, loss_val=None):
    plt.figure(figsize=(16,9))
    plt.plot(loss_train, 'r', label='train_loss')
    if loss_val is not None:
        plt.plot(loss_val, 'b', label='val_loss')
    plt.legend()
    plt.xlabel('iterations')
    plt.ylabel('loss')
    plt.yticks(rotation=0)
    plt.xticks(np.arange(0, loss_train.size, step=1 if loss_train.size < 20 else loss_train.size // 20))
    plt.show()