import numpy as np
import os
import pandas as pd
import torch
from torch.utils.data import Dataset
import torchaudio


class FMAWav(Dataset):
    def __init__(self, size, split, transform=None):
        super().__init__()

        if size not in ['small', 'medium']:
            raise ValueError("Size must be one of: ['small', 'medium']")
        if split not in ['training', 'validation', 'test']:
            raise ValueError("Split must be one of: ['training', 'validation', 'test']")

        self.__root_dir = os.path.join('datasets', 'fma', f'fma_{size}_wav')
        
        self.__files, self.__classes, self.__genres = FMAWav.parse_csv(os.path.join(self.__root_dir, f'fma_{size}_wav.csv'), split)
        self.__transform = transform
        
        assert self.__files.size == self.__classes.size, "Internal error: files and classes must have the same size"

    def __len__(self):
        return self.__files.size
#         return 16

    def __getitem__(self, idx):
        # Load audio file
        x, sr = torchaudio.load(os.path.join(self.__root_dir, self.__files[idx]))

        # Insert second channel if x is mono
        if x.shape[0] == 1:
            x = torch.cat((x, x), 0)

        # Load labels
        label = torch.tensor(self.__classes[idx])

        # Perform transofmations
        if self.__transform:
            return self.__transform(x), label

        return x, label
    
    @property
    def genres(self):
        return self.__genres
    
    @property
    def ground_truth(self):
        return self.__classes
    
    @property
    def class_distribution(self):
        return {class_name: self.__classes[self.__classes == class_idx].size for class_idx, class_name in enumerate(self.__genres)}
    
    @staticmethod
    def parse_csv(csv_path, split):
        df = pd.read_csv(csv_path)
        df = df[df.split == split]
        files = df['file'].to_numpy()
        genres = sorted(list(df['genre'].unique()))
        
        classes = np.array(list(map(lambda genre: genres.index(genre), list(df['genre']))))
        return files, classes, genres