import torch
from torchvision.models import resnet


class TorchismNet(torch.nn.Module):
    def __init__(self, num_classes):
        super().__init__()
        self.__num_classes = num_classes
        self.__modules = torch.nn.Sequential(
            torch.nn.Conv2d(2, num_classes, 3),
            torch.nn.BatchNorm2d(num_classes),
            torch.nn.ReLU(),
            torch.nn.Conv2d(num_classes, 2*num_classes, 3),
            torch.nn.BatchNorm2d(2*num_classes),
            torch.nn.ReLU(),
            torch.nn.Conv2d(2*num_classes, 3*num_classes, 3),
            torch.nn.BatchNorm2d(3*num_classes),
            torch.nn.ReLU(),
            torch.nn.Conv2d(3*num_classes, 5*num_classes, 3),
            torch.nn.BatchNorm2d(5*num_classes),
            torch.nn.ReLU(),
            torch.nn.Conv2d(5*num_classes, 7*num_classes, 3),
            torch.nn.BatchNorm2d(7*num_classes),
            torch.nn.ReLU(),
            torch.nn.Conv2d(7*num_classes, 4*num_classes, 3),
            torch.nn.BatchNorm2d(4*num_classes),
            torch.nn.ReLU(),
            torch.nn.Conv2d(4*num_classes, 2*num_classes, 3),
            torch.nn.BatchNorm2d(2*num_classes),
            torch.nn.ReLU(),
            torch.nn.Conv2d(2*num_classes, num_classes, 3),
            torch.nn.AdaptiveMaxPool2d((1, 1))
        )

    def forward(self, inp):
        return self.__modules(inp).view(1, self.__num_classes)
    

class TorchismNet1d(torch.nn.Module):
    def __init__(self, num_classes):
        super().__init__()
        self.__num_classes = num_classes
        self.__modules = torch.nn.Sequential(
            torch.nn.Conv1d(2, num_classes, 3),
            torch.nn.BatchNorm1d(num_classes),
            torch.nn.ReLU(),
            torch.nn.Conv1d(num_classes, 2*num_classes, 3),
            torch.nn.BatchNorm1d(2*num_classes),
            torch.nn.ReLU(),
            torch.nn.Conv1d(2*num_classes, 3*num_classes, 3),
            torch.nn.BatchNorm1d(3*num_classes),
            torch.nn.ReLU(),
            torch.nn.Conv1d(3*num_classes, 5*num_classes, 3),
            torch.nn.BatchNorm1d(5*num_classes),
            torch.nn.ReLU(),
            torch.nn.Conv1d(5*num_classes, 7*num_classes, 3),
            torch.nn.BatchNorm1d(7*num_classes),
            torch.nn.ReLU(),
            torch.nn.Conv1d(7*num_classes, 4*num_classes, 3),
            torch.nn.BatchNorm1d(4*num_classes),
            torch.nn.ReLU(),
            torch.nn.Conv1d(4*num_classes, 2*num_classes, 3),
            torch.nn.BatchNorm1d(2*num_classes),
            torch.nn.ReLU(),
            torch.nn.Conv1d(2*num_classes, num_classes, 3),
            torch.nn.AdaptiveMaxPool1d(1)
        )

    def forward(self, inp):
        return self.__modules(inp).view(1, self.__num_classes)


class GenreResnet101(torch.nn.Module):
    def __init__(self, num_classes, pretrained=False):
        super().__init__()
        self.__backbone = torch.nn.Sequential(*(list(resnet.resnet101(pretrained=pretrained).children())[:-1]))
        self.__classifier = torch.nn.Linear(2048, num_classes)
        
    def forward(self, x):
        features = self.__backbone(x)
        return self.__classifier(features.flatten(1))
    

class GenreLSTM(torch.nn.Module):
    def __init__(self, num_classes, input_size=2, hidden_dim=20, num_layers=2):
        super().__init__()
        self.__num_classes = num_classes
        self.__input_size = input_size
        self.__hidden_dim = hidden_dim
        self.__num_layers = num_layers
        
        self.__lstm = torch.nn.LSTM(
            input_size=self.__input_size,
            hidden_size=self.__hidden_dim,
            num_layers=self.__num_layers,
            batch_first=True)
        
        self.__classifier = torch.nn.Linear(self.__hidden_dim, self.__num_classes)

    def forward(self, x):
        _, (h, c) = self.__lstm(x, None)
        return self.__classifier(h[-1].view(-1)).unsqueeze(0)


class ResidualBlock1d(torch.nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size=3, stride=1):
        super().__init__()
        self.__block1 = torch.nn.Sequential(
            torch.nn.Conv1d(in_channels, out_channels, kernel_size=kernel_size, stride=stride, padding=1),
            torch.nn.BatchNorm1d(out_channels)
        )
        self.__block2 = torch.nn.Sequential(
            torch.nn.Conv1d(out_channels, out_channels, kernel_size=kernel_size, stride=stride, padding=1),
            torch.nn.BatchNorm1d(out_channels)
        )
        self.__downsample = None
        if in_channels != out_channels or stride != 1:
            self.__downsample = torch.nn.Sequential(
                torch.nn.Conv1d(in_channels, out_channels, 1, stride, bias=False)
            )
        
        self.__relu = torch.nn.ReLU(inplace=True)

    def forward(self, x):
        identity = x if self.__downsample is None else self.__downsample(x)
        out = self.__block1(x)
        out = self.__relu(out)

        out = self.__block2(out)
        identity = identity if self.__downsample is None else self.__downsample(x)
        out = out + identity
        out = self.__relu(out)
        
        return out


class ResTorchism(torch.nn.Module):
    def __init__(self, num_classes, featuremap_size):
        super().__init__()
        self.__num_classes = num_classes

        res_blocks = []
        channels = [2, 64, 64, 128, 128, 256, 256, 256, 256, 256, 256, 256, 512, 512, 512]
        for i in range(len(channels)-1):
            res_blocks.append(ResidualBlock1d(channels[i], channels[i+1]))

        self.__feature_extractor = torch.nn.Sequential(*res_blocks)
        self.__pooling = torch.nn.AdaptiveAvgPool1d(featuremap_size)
        self.__classifier = torch.nn.Linear(channels[-1]*featuremap_size, num_classes)

    def forward(self, x):
        out = self.__feature_extractor(x)
        out = self.__pooling(out)
        out = torch.flatten(out, 1)
        out = self.__classifier(out)
        return out
    
    
class ResTorchism24(torch.nn.Module):
    def __init__(self, num_classes, featuremap_size):
        super().__init__()
        self.__num_classes = num_classes

        res_blocks = []
        channels = [2, 32, 32, 32, 64, 64, 64, 64, 128, 128, 128, 128, 356, 256, 256, 256, 256, 256, 256, 256, 512, 512, 512, 512]
        for i in range(len(channels)-1):
            res_blocks.append(ResidualBlock1d(channels[i], channels[i+1]))

        self.__feature_extractor = torch.nn.Sequential(*res_blocks)
        self.__pooling = torch.nn.AdaptiveAvgPool1d(featuremap_size)
        self.__classifier = torch.nn.Linear(channels[-1]*featuremap_size, num_classes)

    def forward(self, x):
        out = self.__feature_extractor(x)
        out = self.__pooling(out)
        out = torch.flatten(out, 1)
        out = self.__classifier(out)
        return out