# Torchism
Projekat je nastao kao seminarski rad kursa Naucno Izracunavanje sa master studija
Matematičkog fakulteta, univerziteta u Beogradu. Pored samog seminarskog rada, Torchism
će poslužiti kao osnova istraživackog rada koje prethodi mojoj master tezi.

Problem rešavan ovim projektom je klasifikacija muzičkog žanra na osnovu audio zapisa
pesme. Pristup predložen projektom je moja autentična ideja. Preklapanje ideje sa
postojećim rešenjima je moguće isključivo ako moja pretraga za pristupima nije bila
dovoljno detaljna.

## Skup podataka
[Slobodna muzička arhiva (eng. Free Music archive)](https://arxiv.org/abs/1612.01840v3)
je skup podataka objavljen na ISMIR konferenciji 2016. godine sa ciljem da olakša rad
istraživačima iz oblasti pretraživanje informacija u muzici (eng. [music information
retrieval, MIR](https://en.wikipedia.org/wiki/Music_information_retrieval)). Skup se
sastoji od pesama snimljenih za potrebe ovog skupa koje su objavljene pod Creative Commons
licencom. U kreiranju skupa je učestvovalo više od 16000 izvođača koji su više od 100000
pesama snimili u okviru od skoro 15000 albuma. Obim celokupnog skupa je 917GB i sacinjen
je od 343 dana muzike (nešto oko 8500 sati). Ukupno 161 muzički žanr je definisan
hijerarhijskom taksonomijom.

Pored samih audio zapisa, u okviru projekta slobodne muzičke arhive su dostupne i meta
informacije o pesmama poput korisničkih ocena, sviđanja, broja reprodukcija, opisa pesama,
albuma i izvođača itd. Dostupni su takođe i atributi korišćeni u standardnim pristupima
analize zvuka izvučeni [librosa](https://github.com/librosa/librosa) bibliotekom, poput
melodijskog spektrograma, stope promene znaka, spektralnih centroida, spektralnih razlika
itd.

#### Predefinisani podskupovi
* Mala arhiva (8000 pesama, 8 balansiranih žanrova, zapisi od 30s)
* Srednja arhiva (25000 pesama, 16 nebalansiranih žanrova, zapisi od 30s)
* Velika arhiva (106574 pesme, 161 nebalansiran žanr, zapisi od 30s)
* Potpuna arhiva (106574 pesme, 161 nebalansiran žanr, nesečeni zapisi)

#### Predefinisane podele
Svaki podskup dolazi sa predefinisanom podelom podataka na skup za obuku, skup za izbor
modela i skup za testiranje. Značajno je imati ovakvu podelu od strane autora kako bi
poređenje pristupa od strane različitih istraživača bilo relevantno.

#### Podela korišćena u projektu Torchism
Podela za koju sam se odlučio je srednja arhiva. Ograničenja koja rezultuju ovim izborom
su izazovnost nebalansiranog skupa podataka i obim koji bi dozvolio brže iteracije.

## Pristup
Osnovna ideja obrade zvuka koju nisam pronašao zastupljenu u literaturi je korišćenje
jednodimenzione konvolucije primenjenu nad sirovim zvučnim signalom, nalik tome što se
dvodimenziona konvolucija primenjuje nad sirovom fotografijom. Ideja koja deluje prirodno
nema toliku podršku literature. Osnovna verzija Torchism projekta koristi adaptaciju
Rezidualne neuronske mreže koristeći jednodimenzionu konvoluciju. Adaptirao sam ResNet24
arhitekturu koja poseduje 24 rezidualna bloka i time postigao prosečnu tačnost 0.48 (
agregiranu nad 16 nebalansiranih klasa) nakon 100 epoha obučavanja.

## Literatura
1. [Naučno izračunavanje](http://ni.matf.bg.ac.rs/materijali/ni.pdf), Mladen Nikolić, Matematicki fakultet, Univerzitet u Beogradu
1. [Slobodna Muzička Arhiva](https://arxiv.org/abs/1612.01840v3)
1. [Librosa](https://github.com/librosa/librosa)
1. [Deep Residual Learning for Image Recognition](https://arxiv.org/abs/1512.03385)
